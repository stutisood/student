const express = require('express')
const controller = require("../controller/apiMethod")
const router = express.Router();


router.post("/postDetails/:_id", controller.postDetails);
router.get("/getDetails", controller.getDetails);

router.post("/addSubMarks/:_id", controller.addSubMarks);
router.get("getAddSubMarks/:studentId")

router.post("/postCollegeDetail",controller.postCollegeDetail);


router.get("/getByAggregate/:studentId",controller.aggregateTable);

module.exports=router



