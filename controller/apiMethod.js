const bcrypt = require('bcrypt')
const studentModel = require("../database/studentDb");
const subMarksModel = require('../database/marksDb')
const collegeModel = require('../database/collegeDb')

const mongodb = require('mongodb');


module.exports.postDetails = async (req, res) => {
  try {

    const record = await studentModel.create(
      {collegeId : req.params._id,
      firstName :req.body.firstName,
      lastName :req.body.lastName,
      email : req.body.email,
      dialCode :req.body.dialCode,
      phoneNumber : req.body.phoneNumber,
      password : req.body.password}
    )

    return res.json({
      message: "Student Record Added",
      data: record,
    });

  } catch (error) {
    res.status(400).send(error.message);
  }
  
};

module.exports.getDetails = async (req, res) => {  
  try{
 
    let getDetail = await studentModel.find({}).populate('collegeId');
    res.json({getDetail});

      } catch (error) {
    return res.status(400).json({
      msg: "Error fetching student details",
      error: error.message,
    });
  }
};


module.exports.addSubMarks = async(req, res) => {
  try{

      const data = await subMarksModel.create({
      studentId:req.params._id,
      subject: req.body.subject,
      marks: req.body.marks,  
  })
  

    return res.json({
      data
    });
  }
  catch{
    return res.status(400).json({
      msg: "User not found"
    })
  }
};




module.exports.postCollegeDetail = async(req,res) => {
    const getRecord = await collegeModel.create({
      collegeName : req.body.collegeName,
      course: req.body.course
    })
    return res.json({
      message : 'Get Student Record',
      getRecord
    })
  }
  
  module.exports.getCollegeDetailById = async(req,res) => {
    //let id = req.params;
    const getRecordById = await collegeModel.findOne(req.params);
    return res.json({
      message : 'Get Student Record',
      getRecordById
    })
  }

  module.exports.aggregateTable = async(req,res) =>{

    let userid=subMarksModel.ObjectId(req.params);

    const details = await subMarksModel.aggregate([
    {
        $lookup: {
          from: "studentmodels",
          localField: "studentId",
          foreignField: "_id",
          as: "studentmarks",
        },
      },

      {
        $unwind:"studentmarks"
      },

      {
        $lookup: {
          from: "collegemodels",
          localField: "studentmarks.collegeId",
          foreignField: "_id",
          as: "collegestudents",
        },
      },

      {
        $unwind:"collegestudents"
      },

      {
        $match: {studentId:userid}
      },

      {
        $skip:0
      },

      {
        $limit:3
      },

      {
        $sort: {createdAt: -1}
      },

      {
        $project: {
           "studentMarks.password" : false,
           "collegestudents.password" : false
        }
      }

])
return res.json({details})
}