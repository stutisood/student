const mongoose = require('mongoose')


const studentSchema = new mongoose.Schema({
    
    created_at: { type: Date, default: Date.now },

    collegeId: {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'collegeModel'
    },


    firstName: {
        type: String,
        // required: true,
    },

    lastName: {
        type: String,
        // required: true,
    },

    email: {
        type: String,
        // required: true,
    },

    dialCode: {
        type: String,
        // required: true,
    },

    phoneNumber: {
        type: String
        // required: true
    },

    password: {
        type: String,
        // required: true
    },

    jti: {
        type: String
    }
},
{
    toJSON:{
        transform(doc, ret){
            delete ret.password;
        }
    }
}
) 

const studentModel = mongoose.model('studentModel', studentSchema);


module.exports= studentModel