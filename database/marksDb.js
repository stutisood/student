const mongoose = require('mongoose');


const subMarksSchema = new mongoose.Schema({
    studentId: {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'studentModel'
    },

    subject : {
        type : String,
    },

    marks : {
        type : Number,
    }
})

const subMarksModel = mongoose.model('subMarksModel', subMarksSchema);

module.exports = subMarksModel;